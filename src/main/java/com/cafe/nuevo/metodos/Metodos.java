package com.cafe.nuevo.metodos;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cafe.nuevo.Objetos.Order;
import com.cafe.nuevo.Objetos.Orders;
import com.cafe.nuevo.Objetos.Product;
import com.cafe.nuevo.Objetos.ProductApply;
import com.cafe.nuevo.Objetos.Vender;
import com.cafe.nuevo.repositorio.OrderRepository;
import com.cafe.nuevo.repositorio.OrdersRepository;
import com.cafe.nuevo.repositorio.ProductApplyRepo;
import com.cafe.nuevo.repositorio.VenderRepository;


@Service
public class Metodos {
	
	@Autowired
	VenderRepository venderRepository; 
	
	@Autowired
	OrderRepository orderRepository;
	
	@Autowired
	OrdersRepository ordersRepository;
	
	@Autowired
	ProductApplyRepo productApplyRepo;
	
	public Product adddproducto (Product addp) {
		orderRepository.save(addp); 
		return addp; 
	}
	
	public Vender adventa (Vender addve) {
		LocalDate date = LocalDate.now();
		addve.setDate(date); 
		venderRepository.save(addve); 
		Iterable<Product> ingresa = orderRepository.findAllById(addve.getProd());
		List<Product> ingresar = new ArrayList<Product>();
		ingresa.forEach(ingresar::add);
		Integer restotal = 0; 
		List<Integer> result = ingresar.stream().map(Product::getPrice).collect(Collectors.toList());
		for(Integer aun:result ) {
			restotal += aun; 
		}
		Orders guardarOrders = new Orders();
		guardarOrders.setDate(addve.getDate());
		guardarOrders.setId(addve.getId());
		guardarOrders.setProductos(ingresar);
		guardarOrders.setTotalOrder(restotal);
		ordersRepository.save( guardarOrders);
		return addve; 
	}
	
	public Order lookForDate (LocalDate date){
		Iterable<Orders> orde =  ordersRepository.findAllByDate(date);
		List<Orders> orders = new ArrayList<Orders> ();
		orde.forEach(orders::add);
		Integer total = 0; 
		List<Integer> result = orders.stream().map(Orders::getTotalOrder).collect(Collectors.toList());
		for(Integer an:result ) {
			total += an; 
		}
		Order dia = new Order();
		dia.setOrders(orders);
		dia.setTotalday(total);
		return dia; 
	}
	
	public ProductApply addCat (ProductApply addpa) {
		productApplyRepo.save(addpa); 
		return addpa; 
	}
	
	
	
}
