package com.cafe.nuevo.Objetos;

import java.util.List;


public class Order {
	private List<Orders> orders;
	private Integer totalday; 

	
	public Integer getTotalday() {
		return totalday;
	}

	public void setTotalday(Integer totalday) {
		this.totalday = totalday;
	}

	public List<Orders> getOrders() {
		return orders;
	}

	public void setOrders(List<Orders> orders) {
		this.orders = orders;
	} 
	

}
