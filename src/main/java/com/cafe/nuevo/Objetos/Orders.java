package com.cafe.nuevo.Objetos;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection ="orders")
public class Orders {
	private Integer id;
	private List<Product> productos; 
	private Integer totalOrder; 
	private LocalDate date;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id += 1;
	}
	public List<Product> getProductos() {
		return productos;
	}
	public void setProductos(List<Product> productos) {
		this.productos = productos;
	}
	public Integer getTotalOrder() {
		return totalOrder;
	}
	public void setTotalOrder(Integer totalOrder) {
		this.totalOrder = totalOrder;
	}
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	} 
	
}
