package com.cafe.nuevo.Objetos;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection ="vender")
public class Vender {
	
	@Id
	private Integer id; 
	private List<String> prod; 
	private LocalDate date;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public List<String> getProd() {
		return prod;
	}
	public void setProd(List<String> prod) {
		this.prod = prod;
	}
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	} 
	
	
	

}
