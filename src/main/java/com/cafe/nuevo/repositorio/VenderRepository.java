package com.cafe.nuevo.repositorio;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.cafe.nuevo.Objetos.Vender;

public interface VenderRepository extends MongoRepository <Vender,Integer>{
	
	List<Vender> findAllByDate (LocalDate fecha);

}
