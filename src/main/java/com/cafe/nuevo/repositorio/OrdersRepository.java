package com.cafe.nuevo.repositorio;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.cafe.nuevo.Objetos.Orders;

public interface OrdersRepository extends MongoRepository<Orders,String>{
	List<Orders> findAllByDate (LocalDate fecha);

}
