package com.cafe.nuevo.repositorio;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.cafe.nuevo.Objetos.Product;

public interface OrderRepository extends MongoRepository<Product,String>{

}
