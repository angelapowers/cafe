package com.cafe.nuevo.controller;

import java.time.LocalDate;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cafe.nuevo.Objetos.Order;
//import com.cafe.nuevo.Objetos.Orders;
import com.cafe.nuevo.Objetos.Product;
import com.cafe.nuevo.Objetos.ProductApply;
import com.cafe.nuevo.Objetos.Vender;
import com.cafe.nuevo.metodos.Metodos;

@RestController
public class Controller {
	
	@Autowired
	Metodos metodos; 
	
	@PostMapping("/vender")
	public Vender addventa (@RequestBody Vender addventa) {
		return metodos.adventa(addventa);
	}
	@PostMapping("/registrarProducto")
	public Product addProd (@RequestBody Product addprod) {
		return metodos.adddproducto(addprod); 
	}
	@GetMapping("/consultarVentas/{date}")
	public Order ventaDia (@PathVariable("date") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date) {     	   
		return metodos.lookForDate(date); 
	}
	@PostMapping("/registraProductoCategoria")
	public ProductApply addcate(@RequestBody ProductApply addpap) {
		return metodos.addCat(addpap);
	}
	

}
